package cn.mrx.eas.shiro;

import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.dto.sys.SysUser;
import cn.mrx.eas.server.Const;
import cn.mrx.eas.service.sys.ISysPermissionService;
import cn.mrx.eas.service.sys.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Author: Mr.X
 * Date: 2017/12/24 上午11:28
 * Description:
 */
@Slf4j
public class EasRealm extends AuthorizingRealm {

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysPermissionService iSysPermissionService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        log.info("Shiro授权开始...");
        SysUser sysUser = (SysUser) principalCollection.getPrimaryPrincipal();
        List<String> permissionUrlList = new ArrayList<>();
        // admin用户拥有所有权限,其他用户则从数据库查询应有的权限
        if (sysUser.getUsername().equals(Const.ADMIN_USERNAME)) {
            List<SysPermission> sysPermissionList = iSysPermissionService.findAll();
            for (SysPermission sysPermission : sysPermissionList) {
                permissionUrlList.add(sysPermission.getUrl());
            }
        } else {
            List<SysPermission> sysPermissionList = iSysPermissionService.findAllByUserId(sysUser.getId());
            for (SysPermission sysPermission : sysPermissionList) {
                permissionUrlList.add(sysPermission.getUrl());
            }
        }
        // 权限去重
        Set<String> permissionUrlSet = new HashSet<>();
        for (String permissionUrl : permissionUrlList) {
            if (StringUtils.isBlank(permissionUrl)) {
                continue;
            }
            permissionUrlSet.addAll(Arrays.asList(permissionUrl.trim().split(",")));
        }
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        log.info("权限列表:{}", permissionUrlSet);
        info.setStringPermissions(permissionUrlSet);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        log.info("Shiro认证开始...");
        String username = (String) token.getPrincipal();
        SysUser sysUser = iSysUserService.findByUsername(username);
        if (sysUser == null) {
            return null;
        }
        return new SimpleAuthenticationInfo(sysUser, sysUser.getPassword(), ByteSource.Util.bytes(Const.SALT), getName());
    }

    public void clearCached() {
        log.info("Shiro清除缓存...");
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCache(principals);
    }
}