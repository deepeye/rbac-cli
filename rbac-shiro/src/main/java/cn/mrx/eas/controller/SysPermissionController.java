package cn.mrx.eas.controller;

import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.exception.EasException;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.service.sys.ISysPermissionService;
import cn.mrx.eas.vo.sys.TreeNode;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 下午2:18
 * Description:
 */
@Controller
@RequestMapping("/sys/permission")
public class SysPermissionController {

    @Autowired
    private ISysPermissionService iSysPermissionService;

    // 权限列表页面
    @RequiresPermissions("sys:permission:list")
    @GetMapping
    public String index() {
        return "sys/permission";
    }

    // 分页查询权限逻辑
    @RequiresPermissions("sys:permission:list")
    @PostMapping("/list")
    @ResponseBody
    public BSGrid list(Integer curPage, Integer pageSize, String name) {
        return iSysPermissionService.permissionListPage(curPage, pageSize, name);
    }

    // 编辑权限页面
    @RequiresPermissions("sys:permission:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model) {
        SysPermission sysPermission = iSysPermissionService.findOneById(id);
        model.addAttribute("sysPermission", sysPermission);
        return "sys/permission-edit";
    }

    // 获取所有权限节点(添加角色/添加权限/修改权限页面会用到)
    @RequiresPermissions("sys:permission:allNodes")
    @PostMapping("/allNodes")
    @ResponseBody
    public List<TreeNode> allNodes() {
        return iSysPermissionService.allNodes();
    }

    // 编辑权限逻辑
    @RequiresPermissions("sys:permission:edit")
    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse edit(SysPermission sysPermission) {
        return iSysPermissionService.editPermission(sysPermission);
    }

    // 添加权限页面
    @RequiresPermissions("sys:permission:add")
    @GetMapping("/add")
    public String add() {
        return "sys/permission-add";
    }

    // 添加权限逻辑
    @RequiresPermissions("sys:permission:add")
    @PostMapping("/add")
    @ResponseBody
    public ServerResponse add(SysPermission sysPermission) {
        return iSysPermissionService.addPermission(sysPermission);
    }

    // 批量删除权限逻辑
    @RequiresPermissions("sys:permission:batchDel")
    @PostMapping("/batchDel")
    @ResponseBody
    public ServerResponse batchDel(String ids) {
        return iSysPermissionService.batchDel(ids);
    }
}