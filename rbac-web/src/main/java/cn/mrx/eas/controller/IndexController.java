package cn.mrx.eas.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Author: Mr.X
 * Date: 2017/12/23 下午5:48
 * Description:
 */
@Controller
@RequestMapping
public class IndexController {

    @GetMapping
    public String index() {
        return "redirect:/admin";
    }

    @GetMapping("/admin")
    public String admin() {
        return "index";
    }

    @GetMapping("/welcome")
    public String welcome() {
        return "welcome";
    }

    @RequiresPermissions("404Test")
    @GetMapping("/404Test")
    public String _404Test() {
        // 返回一个不存在的视图
        return "abc";
    }

    @RequiresPermissions("500Test")
    @GetMapping("/500Test")
    public String _500Test() {
        // 自定义ArithmeticException异常
        int num = 5 / 0;
        return "";
    }

    @RequiresPermissions("stress1")
    @GetMapping("/stress1")
    public String stress1() {
        return "stress/stress1";
    }

    @RequiresPermissions("stress2")
    @GetMapping("/stress2")
    public String stress2() {
        return "stress/stress2";
    }

    @RequiresPermissions("stress3")
    @GetMapping("/stress3")
    public String stress3() {
        return "stress/stress3";
    }

    @RequiresPermissions("stress4")
    @GetMapping("/stress4")
    public String stress4() {
        return "stress/stress4";
    }
}