package cn.mrx.eas;

import cn.mrx.eas.dao.sys.SysUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Author: Mr.X
 * Date: 2017/12/6 下午3:06
 * Description:
 */
@Slf4j
public class UserMapperDemo1 {

    private ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[]{"classpath:/spring/applicationContext-service.xml", "classpath:/spring/applicationContext-dao.xml"});

    @Test
    public void test01() {
        SysUserMapper userMapper = applicationContext.getBean(SysUserMapper.class);
        //log.info("根据主键获取用户信息==>{}", userMapper.selectByPrimaryKey(1));
    }
}
