# rbac-cli

权限脚手架系统

一款基于Spring+SpringMVC+Mybatis+Shiro的一个权限脚手架，用户可在此基础上很方便的进行二次开发！

# 技术特色

- Spring
- SpringMVC
- Mybatis
- pagehelper
- druid
- shiro
- kaptcha
- lombok
- logback
- freemarker+jsp
- commons工具包
- guava

# 界面预览

![1](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/1.png)
![2](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/2.png)
![3](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/3.png)
![4](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/4.png)
![5](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/5.png)
![6](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/6.png)
![7](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/7.png)
![8](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/8.png)
![9](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/9.png)
![10](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/10.png)
![11](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/11.png)
![12](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/12.png)
![13](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/13.png)
![14](https://raw.githubusercontent.com/bobi1234/eas/v1.0/doc/readme/14.png)
