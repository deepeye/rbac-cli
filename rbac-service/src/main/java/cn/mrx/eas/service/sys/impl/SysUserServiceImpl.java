package cn.mrx.eas.service.sys.impl;

import cn.mrx.eas.dao.sys.SysRoleMapper;
import cn.mrx.eas.dao.sys.SysUserMapper;
import cn.mrx.eas.dao.sys.SysUserRoleMapper;
import cn.mrx.eas.dto.sys.SysUser;
import cn.mrx.eas.exception.EasException;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.service.sys.ISysUserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/23 下午5:26
 * Description:
 */
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public BSGrid userListPage(Integer curPage, Integer pageSize) {
        Page<SysUser> sysUserPage = PageHelper.startPage(curPage, pageSize);
        List<SysUser> sysUserList = sysUserMapper.userListPage();
        for (SysUser sysUser : sysUserList) {
            sysUser.setPassword(StringUtils.EMPTY);
            sysUser.setSysRoleList(sysRoleMapper.findAllByUserId(sysUser.getId()));
        }
        return new BSGrid(true, curPage, pageSize, sysUserPage.getTotal(), null, null, sysUserList);
    }

    @Override
    public SysUser findByUsername(String username) {
        return sysUserMapper.findByUsername(username);
    }

    @Override
    public SysUser findOneById(Integer id) {
        SysUser sysUser = sysUserMapper.findOneById(id);
        if (sysUser == null) {
            throw new EasException(404, "没有该条数据");
        }
        return sysUser;
    }

    @Transactional
    @Override
    public ServerResponse editUser(SysUser sysUser, Integer[] roleIds) {
        // 修改用户基本信息
        sysUserMapper.editUser(sysUser);
        // 删除用户所拥有的角色
        sysUserRoleMapper.deleteRoleByUserId(sysUser.getId());
        // 重新赋予用户新角色
        int result = sysUserRoleMapper.assignRole(sysUser.getId(), roleIds);
        if (result > 0) return ServerResponse.success("修改成功");
        return ServerResponse.error("修改失败");
    }

    @Transactional
    @Override
    public ServerResponse addUser(SysUser sysUser, Integer[] roleIds) {
        // 添加用户基本信息
        sysUserMapper.addUser(sysUser);
        // 赋予用户角色,注意:sysUser.getId()是插入成功后返回的主键值
        int result = sysUserRoleMapper.assignRole(sysUser.getId(), roleIds);
        if (result > 0) return ServerResponse.success("添加成功");
        return ServerResponse.error("添加失败");
    }

    @Transactional
    @Override
    public ServerResponse batchDel(String ids) {
        String[] idArr = ids.split(",");
        // 删除用户
        int result = sysUserMapper.batchDel(idArr);
        // 把该用户对应的中间表数据也删除掉
        for (String id : idArr) {
            sysUserRoleMapper.deleteRoleByUserId(Integer.valueOf(id));
        }
        if (result > 0) return ServerResponse.success("批量操作成功");
        return ServerResponse.error("批量操作失败");
    }
}